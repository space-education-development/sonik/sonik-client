import json
import logging

LOGGER = logging.getLogger(__name__)


class Artifacts():  # pylint: disable=R0903

    def __init__(self, waterfall, metadata):
        """Constructor.

        Arguments:
            waterfall_data: The Waterfall object to be stored
            metadata: A JSON-serializeable dictionary, structure:
                      {
                        'observation_id': integer number,
                        'tle': 3-line string,
                        'frequency': integer number,
                        'location': {
                          'latitude': number,
                          'longitude': number,
                          'altitude': self.location['elev']
                        }
                      }
        """
        self.artifacts_file = None
        self._waterfall_data = waterfall.data
        self._metadata = json.dumps(metadata)

    def create(self):
        pass
